
port forwarding:

kubectl port-forward service/argocd-server -n argocd 8080:80

kubectl port-forward service/statichelloworld-service -n helloworld  8850:80

kubectl port-forward service/statichelloworld-service -n helloworld-staging  8860:80